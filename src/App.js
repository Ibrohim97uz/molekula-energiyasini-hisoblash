import { Button, Paper } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { motion } from "framer-motion";
import { useForm, FormProvider } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import "./App.css";
import FormInput from "./components/shared/FormInput";
import formula from "./reactor";
import { useState } from "react";
import Chart from "react-apexcharts";

const useStyles = makeStyles({
  form: {
    "&>div": {
      width: "70%",
      margin: "0 auto",
      "@media (max-width:800px)": {
        width: "100%",
      },
    },
  },
  container: {
    width: "90%",
    margin: "0 auto",
  },
});

const values = [
  { name: "L", description: "Световую длину лампы", measure: "mm" },
  { name: "R", description: "Внутренний радиус реактора", measure: "mm" },
  { name: "r", description: "Наружный радиус лампы", measure: "mm" },
  { name: "Q", description: "Расход воды через реактор", measure: "м^3/час" },
  { name: "W", description: "Электрическая энергия", measure: "Дж" },
  { name: "f", description: "Частота следования импульсов", measure: "Hr" },
  { name: "n", description: "Спектральный КПД лампы", measure: "" },
  {
    name: "A",
    description: "Спектральный коэффициент поглощения обрабатываемой воды ",
    measure: "см^-1",
  },
];

const schemaObject = {};

values.forEach(
  (v) =>
    (schemaObject[v.name] = yup
      .number()
      .typeError("Amount must be a number")
      .required(v.description + " обязательно!"))
);

function App() {
  const classes = useStyles();
  const schema = yup.object().shape(schemaObject);
  const [result, setResult] = useState({
    general: "",
    forChart: null,
    open: false,
  });

  const methods = useForm({
    resolver: yupResolver(schema),
    mode: "onChange",
  });

  const onSubmit = (data) => {
    const result = formula(
      +data.L,
      +data.R,
      +data.r,
      +data.Q,
      +data.W,
      +data.f,
      +data.n,
      +data.r,
      +data.A
    );
    setResult({ ...result, open: true });
  };

  return (
    <div className="App">
      <div className={classes.container}>
        <div className="title-wrapper">
          <div className="title">
            <motion.h1
              initial={{ scale: 0 }}
              animate={{ scale: 1, transition: { delay: 0.3 } }}
            >
              ПЛАЗМА
            </motion.h1>
          </div>
        </div>
        <FormProvider {...methods}>
          <motion.form
            initial={{ scale: 0 }}
            animate={{ scale: 1, transition: { delay: 0.6 } }}
            onSubmit={methods.handleSubmit(onSubmit)}
            className={classes.form}
          >
            <Paper elevation={3} style={{ padding: "30px" }}>
              {values.map((v, i) => (
                <FormInput
                  key={i}
                  name={v.name}
                  label={v.description}
                  measure={v.measure}
                />
              ))}
              <motion.div>
                <Button
                  style={{ marginTop: "30px" }}
                  type="submit"
                  variant="contained"
                  fullWidth
                >
                  Считать
                </Button>
              </motion.div>
            </Paper>
          </motion.form>

          {result.open ? (
            <motion.div style={{ textAlign: "center" }}>
              <h1>{result.general}</h1>

              {/* <Paper sx={{ width: "700px", margin: "0 auto" }} elevation={3}>
                <Chart
                  options={{
                    chart: {
                      id: "basic-bar",
                    },
                    xaxis: {
                      koeffisientlar: result?.forChart?.koeffisientlar,
                    },
                  }}
                  series={[
                    {
                      name: "Доза",
                      data: result?.forChart?.dozalar,
                    },
                  ]}
                  type="line"
                  width="100%"
                />
              </Paper> */}
            </motion.div>
          ) : null}
        </FormProvider>
      </div>
    </div>
  );
}

export default App;
