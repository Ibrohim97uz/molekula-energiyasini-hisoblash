"use strict";

const main = (L, R, r, Q, W, f, ηΔλ, A) => {
  L = L / 1000;
  R = R / 1000;
  r = r / 1000;
  Q = Q / 3600;
  A = A * 100;

  function n(L, R, r) {
    return Math.trunc((5 * L) / (R - r));
  }

  function v(Q, R, r) {
    return Q / (Math.PI * (R ** 2 - r ** 2));
  }

  function EΔλ(ηΔλ, W) {
    return ηΔλ * W;
  }

  function T() {
    return EΔλ(ηΔλ, W) / n(L, R, r);
  }

  function 𝑋0(f) {
    return v(Q, R, r) / f;
  }

  function D() {
    let umumiyDoza = 0;

    for (let k = 1; k <= Math.trunc(L / 𝑋0(f)); k++) {
      let doza = 0;
      if (k * 𝑋0(f) <= L) {
        for (let i = 1; i <= n(L, R, r); i++) {
          const fR = R ** 2;
          const fX0 = k * 𝑋0(f);
          const fL = L;
          const fi = 2 * i - 1;
          const f2n = 2 * n(L, R, r);

          // const bi =
          //   (R ** 2 +
          //     (k * 𝑋0(f) - ((L * (2 * i - 1)) / 2* n(L, R, r)))  ** 2) ** 1 /2;

          const bi = (fR + (fX0 - (fL * fi) / f2n) ** 2) ** 1 / 2;

          doza = doza + (T() * Math.E ** (-A * bi)) / (4 * Math.PI * bi ** 2);
        }
      }

      umumiyDoza += doza;
    }

    return umumiyDoza;
  }

  const koeffisientlarA = [];
  const dozalarD = [];

  function forChart() {
    for (let i = 0; i < 30; i++) {
      A = i;
      koeffisientlarA.push(A);
      dozalarD.push(D() / (10000 * 4.5));
    }
    return {
      koeffisientlar: koeffisientlarA,
      dozalar: dozalarD,
    };
  }

  return {
    general: `Доза биоцидного излучения D0 =${D() / (10000 * 4.5)} мДж/см2`,
    forChart: forChart(L, R, r, Q, W, f, ηΔλ),
  };
};

//For diagram
// -> A(0 -- 0.3)  ----> result D

export default main;
