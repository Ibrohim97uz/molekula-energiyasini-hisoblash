import { useFormContext, Controller } from "react-hook-form";
import { TextField, InputAdornment, FormControl } from "@mui/material";
import ErrorMessage from "./ErrorMessage";

export default function FormInput({ name, label, measure }) {
  const {
    control,
    formState: { errors },
  } = useFormContext();

  return (
    <div style={{ marginTop: "20px" }}>
      <Controller
        name={name}
        control={control}
        render={({ field }) => (
          <FormControl required fullWidth>
            <TextField
              {...field}
              label={label}
              variant="filled"
              fullWidth
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">{measure}</InputAdornment>
                ),
              }}
            />
            <ErrorMessage error={errors[name]?.message} />
          </FormControl>
        )}
      />
    </div>
  );
}
